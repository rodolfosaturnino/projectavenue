package org.rodolfo.ProjectAvenue.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;
import org.rodolfo.ProjectAvenue.resources.ProductResources;

@Configuration
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        register(ProductResources.class);
    }
}


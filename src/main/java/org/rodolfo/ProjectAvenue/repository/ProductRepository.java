package org.rodolfo.ProjectAvenue.repository;

import org.rodolfo.ProjectAvenue.model.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long>{

}

package org.rodolfo.ProjectAvenue.repository;

import org.rodolfo.ProjectAvenue.model.Image;
import org.springframework.data.repository.CrudRepository;

public interface ImageRepository extends CrudRepository<Image, Long>{

}

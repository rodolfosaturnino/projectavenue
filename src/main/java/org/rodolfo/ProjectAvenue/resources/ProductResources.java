package org.rodolfo.ProjectAvenue.resources;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.rodolfo.ProjectAvenue.model.Image;
import org.rodolfo.ProjectAvenue.model.Product;
import org.rodolfo.ProjectAvenue.model.SimplifiedProduct;
import org.rodolfo.ProjectAvenue.repository.ImageRepository;
import org.rodolfo.ProjectAvenue.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Path("/product")
public class ProductResources {

	@Autowired
    ProductRepository productRepository;
	
	@Autowired
    ImageRepository imageRepository;
	
	@POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Product saveProduct(Product product){
        return productRepository.save(product);
    }

    //READ and
    //d) Same as 2 using specific product identity
    @Path("{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Product getSpecificProduct(@PathParam("id") Long id){
        return productRepository.findOne(id);
    }

    //UPDATE
    @Path("{id}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Product updateProduct(@PathParam("id") Long id, Product p){
        Product product = new Product(id, p.getName(), p.getDescription(), p.getParentProduct());
        return productRepository.save(product);
    }

    //DELETE
    @Path("{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteProduct(@PathParam("id") Long id){
        if(productRepository.exists(id)){
            productRepository.delete(id);
        }
    }

//    a) Get all products excluding relationships (child products, images)
    @Path("all/norelation")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Iterable<SimplifiedProduct> getAllProductsNoRelationship(){
        ArrayList<SimplifiedProduct> alProduct = new ArrayList<>();
        productRepository.findAll().forEach(x->{
            alProduct.add(new SimplifiedProduct(x.getId(),x.getName(),x.getDescription()));
        });
        return alProduct;
    }

//    b) Get all products including specified relationships (child product and/or images)
    @Path("all")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Iterable<Product> getAllProducts(){
        return productRepository.findAll();
    }

//    c) Same as 1 using specific product identity
    @Path("{id}/norelation")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public SimplifiedProduct getSpecificProductNoRelationship(@PathParam("id") Long id){
        Product p = productRepository.findOne(id);
        if(p != null){
            return new SimplifiedProduct(p.getId(),p.getName(),p.getDescription());
        }
        return null;
    }

//    e) Get set of child products for specific product
    @Path("{id}/child")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Iterable<Product> getSpecificChildsProduct(@PathParam("id") Long id){
        Product p = productRepository.findOne(id);
        if(p != null){
            return p.getProductList();
        }
        return null;
    }

//    f) Get set of images for specific product
    @Path("{id}/images")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Iterable<Image> getSpecificImagesProduct(@PathParam("id") Long id){
        Product p = productRepository.findOne(id);
        if(p != null){
            return p.getImageList();
        }
        return null;
    }
    
    //CREATE IMAGE
    @POST
    @Path("/image")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Image saveProduct(Image image){
        return imageRepository.save(image);
    }

    //READ IMAGE
    @Path("/image/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Image getSpecificImage(@PathParam("id") Long id){
        return imageRepository.findOne(id);
    }

    //UPDATE IMAGE
    @Path("/image/{id}")
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Image updateImage(@PathParam("id") Long id, Image image){
        Image im = new Image(id, image.getType(), image.getProduct_id());
        return imageRepository.save(im);
    }

    //DELETE IMAGE
    @Path("/image/{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteImage(@PathParam("id") Long id){
        if(imageRepository.exists(id)){
            imageRepository.delete(id);
        }
    }

}
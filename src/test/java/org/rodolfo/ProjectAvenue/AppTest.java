package org.rodolfo.ProjectAvenue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.rodolfo.ProjectAvenue.model.Image;
import org.rodolfo.ProjectAvenue.model.Product;
import org.rodolfo.ProjectAvenue.model.SimplifiedProduct;
import org.rodolfo.ProjectAvenue.resources.ProductResources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AppTest {

	@Autowired
	ProductResources productResources;

	@Before
	public void setUp() {
	}

	@After
	public void tearDown(){
		productResources.deleteProduct(99l);
		productResources.deleteProduct(899l);
		productResources.deleteImage(199l);
		productResources.deleteImage(299l);
		productResources.deleteImage(699l);
	}

	@Test
	public void productCreateTest(){
		Product product = new Product(99l,"Dog", "Golden Retriever", productResources.getSpecificProduct(2l));
		productResources.saveProduct(product);
		assertEquals(productResources.getSpecificProduct(99l).getName(), "Dog");
		assertEquals(productResources.getSpecificProduct(2l).getProductList().get(0).getName(),"Dog");
	}

	@Test
	public void productReadTest(){
		Product product = productResources.getSpecificProduct(1l);
		assertEquals(product.getDescription(), "Glue Tube");
	}

	@Test
	public void productUpdateTest(){
		Product product = new Product(899l,"Dog", "Dalmata", productResources.getSpecificProduct(2l));
		productResources.saveProduct(product);
		assertEquals(productResources.getSpecificProduct(899l).getName(), "Dog");
		Product oldProduct = productResources.getSpecificProduct(899l);
		Product newProduct = new Product(oldProduct.getId(),"Basset",oldProduct.getDescription(),oldProduct.getParentProduct());
		productResources.updateProduct(newProduct.getId(), newProduct);
		assertEquals(productResources.getSpecificProduct(newProduct.getId()).getName(), "Basset");
	}

	@Test
	public void productDeleteTest(){
		Product product = new Product(999l,"Test Product", "Deletion Test Product", productResources.getSpecificProduct(2l));
		productResources.saveProduct(product);
		assertEquals(productResources.getSpecificProduct(999l).getName(), "Test Product");
		productResources.deleteProduct(999l);
		assertNull(productResources.getSpecificProduct(999l));
	}

	@Test
	public void getAllProductsTest(){
		ArrayList<Product> al = (ArrayList<Product>) productResources.getAllProducts();
		assertNotNull(al);
	}

	@Test
	public void getAllProductsNoRelationshipTest(){
		ArrayList<SimplifiedProduct> al = (ArrayList<SimplifiedProduct>) productResources.getAllProductsNoRelationship();
		assertNotNull(al);
	}

	@Test
	public void getSpecificProductNoRelationshipTest(){
		SimplifiedProduct product= productResources.getSpecificProductNoRelationship(1l);
		assertNotNull(product);
	}

	@Test
	public void getSpecificChildsProductTest(){
		Iterable<Product> childsProducts = productResources.getSpecificChildsProduct(1l);
		assertNotNull(childsProducts);
	}

	@Test
	public void getSpecificImagesProductTest(){
		Iterable<Image> imagesList = productResources.getSpecificImagesProduct(1l);
		assertNotNull(imagesList);
	}

	@Test
	public void imageCreateTest(){
		Image image = new Image(199l, "Test Image", 3l);
		productResources.saveProduct(image);
		assertEquals(productResources.getSpecificImage(199l).getType(), "Test Image");
	}

	@Test
	public void imageReadTest(){
		Image image = productResources.getSpecificImage(1l);
		assertEquals(image.getType(), "tube");
	}

	@Test
	public void imageUpdateTest(){
		Image image = new Image(299l, "Update Test Image", 2l);
		productResources.saveProduct(image);
		Image updatedImage = productResources.getSpecificImage(299l);
		assertEquals(updatedImage.getType(), "Update Test Image");
		Image newImage = new Image(updatedImage.getId(), "Type Image was updated!", 2l);
		productResources.updateImage(newImage.getId(), newImage);
		assertEquals(productResources.getSpecificImage(newImage.getId()).getType(), "Type Image was updated!");
	}

	@Test
	public void imageDeleteTest(){
		Image image = new Image(399l, "Deletion Test Image", 1l);
		productResources.saveProduct(image);
		Image createdImage = productResources.getSpecificImage(399l);
		assertEquals(createdImage.getType(), "Deletion Test Image");
		productResources.deleteImage(createdImage.getId());
		assertNull(productResources.getSpecificProduct(399l));
	}

	@Test
	public void changingImageOwnerTest(){
		Image image = new Image(699l, "Update Test Image", 1l);
		productResources.saveProduct(image);
		Image updatedImage = productResources.getSpecificImage(699l);
		assertEquals(updatedImage.getType(), "Update Test Image");
		Image newImage = new Image(updatedImage.getId(), "Type Image was updated!", 2l);
		productResources.updateImage(newImage.getId(), newImage);
		assertEquals(productResources.getSpecificImage(newImage.getId()).getProduct_id(), (Long)2l);
	}
}
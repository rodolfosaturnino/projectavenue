<<<<<<< HEAD
## Introduction

This project is sample project for building a RESTful service using JAX-RS that depends on [Spring Boot](http://projects.spring.io/spring-boot/), [Jersey](https://jersey.java.net/) and [H2](http://www.h2database.com/html/main.html) projects.

Scenario:

We have a Product Entity with One to Many relationship with Image entity.

Product also has a Many to One relationship with itself (Many Products to one Parent Product).

## Requirements

1º Build a Restful service using JAX-RS to perform CRUD operations on a Product resource using Image as a sub-resource of Product.

2º Your API classes should perform these operations:
1) Create, update and delete products
2) Create, update and delete images
3) Get all products excluding relationships (child products, images) 
4) Get all products including specified relationships (child product and/or images) 
5) Same as 3 using specific product identity 
6) Same as 4 using specific product identity 
7) Get set of child products for specific product 
8) Get set of images for specific product

3º Build JPA/Hibernate classes using annotations to persist these objects in the database 

## Other Requirements

As Spring Boot helps a lot with the CRUD operations a natural path is perform Integration Tests instead Unit Tests since mocking would take a large part on it.
I created a class called [ApplicationTests.java]().
They test all operations from both CRUD, Product and Image, and the other relevant methods.
To run automated tests, go to root folder and execute the following command on your terminal:

## Running the application

Make sure you do not have any server running on port 8080, that is where Tomcat will make available our application. To run, go to root folder and type on your terminal:

> mvn spring-boot:run

## Tests

As Spring Boot helps a lot with the CRUD operations a natural path is perform Integration Tests instead Unit Tests since mocking would take a large part on it.
I created a class called [ApplicationTests.java]().
They test all operations from both CRUD, Product and Image, and the other relevant methods.
To run automated tests, go to root folder and execute the following command on your terminal:

> mvn test

## Executing REST API commands

Use applications to perform the following HTTP calls on your terminal. Just like **curl**

### Product CRUD:

> **CREATE:** curl -i -X POST -H "Content-Type:application/json" -d '{"id": "4","name": "New product 4","description": "A test for posting new products!", "parentProduct" : {"id":2,"name":"Glue in a can","description":"Glue can"} }' localhost:8080/product

> **READ:** curl localhost:8080/product/2

> **UPDATE:** curl -i -X PUT -H "Content-Type:application/json" -d '{ "name" : "Product 4", "description": "A test for posting new products!", "parentProduct" : {"id":3,"name":"Negresco crackers","description":"Crackers"}  }' localhost:8080/product/4

> **DELETE:** curl -i -X DELETE -H "Content-Type:application/json" -d '{}' localhost:8080/product/4

### Image CRUD:

> **CREATE:** curl -i -X POST -H "Content-Type:application/json" -d '{"id": "4","type": "colored", "product_id": "2"}' localhost:8080/product/image

> **READ:** curl localhost:8080/product/image/4

> **UPDATE:** curl -i -X PUT -H "Content-Type:application/json" -d '{"type": "black&white", "product_id": "3"}' localhost:8080/product/image/4

> **DELETE:** curl -i -X DELETE -H "Content-Type:application/json" -d '{}' localhost:8080/product/image/4

### Other requested commands:

>**a) Get all products excluding relationships (child products, images):**
> curl localhost:8080/product/all/norelation

>**b) Get all products including specified relationships (child product and/or images):**
> curl localhost:8080/product/all

>**c) Same as 1 using specific product identity:**
> curl localhost:8080/product/1/norelation

>**d) Same as 2 using specific product identity:**
> curl localhost:8080/product/1

>**e) Get set of child products for specific product:**
> curl localhost:8080/product/1/child

>**f) Get set of images for specific product:**
> curl localhost:8080/product/1/images
=======
# ProjectAvenue

>>>>>>> refs/remotes/origin/master
